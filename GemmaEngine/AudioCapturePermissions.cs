﻿using System;
using System.Threading.Tasks;
using Windows.Media.Capture;
using Windows.UI.Popups;

namespace Gemma
{
    public class AudioCapturePermissions
    {
        /// <summary>
        /// If no recording device is attached, attempting to get access to audio capture
        /// devices will throw an exceptioni with this HResult value
        /// </summary>
        private static int NoCaptureDeviceHResult = -1072845856;

        /// <summary>
        /// Users are prompted to give permission to use capture devices on a per-app basis.
        /// Along with declaring the microphone DeviceCapability in Package.appxmanifest, 
        /// this method tests the privacy setting for microphone access for this application.
        /// Only checks the Settings -> Privacy -> Microphone setting. Does not handle the
        /// Cortana/Dictation privacy check under Settings -> Privacy -> Speech, Inking & Typing.
        /// 
        /// Ideally, the UI code would perform this check every time the app gains focus, as
        /// users may change the privacy settings while the app is in background.
        /// </summary>
        /// <returns></returns>
        public async static Task<bool> RequestMicrophonePermission() {
            try {
                MediaCaptureInitializationSettings settings = new MediaCaptureInitializationSettings();
                settings.StreamingCaptureMode = StreamingCaptureMode.Audio;
                settings.MediaCategory = MediaCategory.Speech;
                MediaCapture capture  = new MediaCapture();

                await capture.InitializeAsync(settings);
            }
            catch (TypeLoadException) {
                // On Windows SKUs without media player (e.g. the N SKUs sold in Europe), we
                // may not have access to the Windows.Media.Capture namespace unless the media
                // player pack is installed.
                var messageDialog = new MessageDialog("Media player components are unavailable.");
                await messageDialog.ShowAsync();
                return false;
            }
            catch (UnauthorizedAccessException) {
                // The user has turned off access to the microphone
                return false;
            }
            catch (Exception exception) {
                if (exception.HResult == NoCaptureDeviceHResult) {
                    var messageDialog = new MessageDialog("No audio capture device detected.");
                    await messageDialog.ShowAsync();
                    return false;
                }
                else {
                    throw;
                }
            }
            return true;
        }
    }
}
