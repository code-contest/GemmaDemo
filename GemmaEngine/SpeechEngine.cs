﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Resources.Core;
using Windows.Globalization;
using Windows.Media.SpeechRecognition;
using Windows.Storage;
using Windows.UI.Core;

namespace Gemma
{
    class SpeechEngine
    {
        /// <summary>
        /// Recognition raises events from the background thread. Calling UIDelegate methods from the background
        /// thread will crash the app. We'll use this to update UI in a thread-safe manner.
        /// </summary>
        private CoreDispatcher uiDispatcher;

        private async Task notifyUI(string message, string messageType = "speechEngineResponse")
        {
            await this.uiDispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (messageType == "speechEngineResponse")
                {
                    UIDelegate.HandleSpeechEngineResponse(message);
                }
                else
                {
                    UIDelegate.HandleUserVoiceInput(message);
                }
                
            });
        }

        private StringBuilder speechCommandLog;

        public async Task Setup(CoreDispatcher uiDispatcher)
        {
            this.uiDispatcher = uiDispatcher;
            bool permissionGranted = await AudioCapturePermissions.RequestMicrophonePermission();

            if (permissionGranted)
            {
                Language speechLanguage = SpeechRecognizer.SystemSpeechLanguage;
                string languageTag = speechLanguage.LanguageTag;
                this.resourceContext = ResourceContext.GetForCurrentView();
                this.resourceContext.Languages = new string[] { languageTag };
                this.resourceMap = ResourceManager.Current.MainResourceMap.GetSubtree("LocalizationSpeechResources");

                // Configure language packs
                this.isFindingLanguagePacks = true;
                IEnumerable<Language> installedLanguages = SpeechRecognizer.SupportedGrammarLanguages;
                UIDelegate.UpdateLanguageComboBox(installedLanguages, speechLanguage);
                this.isFindingLanguagePacks = false;

                // Initialise the recogniser
                await ConfigureRecognizer(SpeechRecognizer.SystemSpeechLanguage);
            }
            else
            {
                await this.notifyUI("Permission to access capture resources was not given by the user, reset the application" +
                                 " setting in Settings -> Privacy -> Microphone.");
            }
        }

        public ISpeechProviderUIDelegate UIDelegate { get; set; }

        /// <summary>
        /// Defines the context within which the app should look for
        /// localization resources
        /// </summary>
        private ResourceContext resourceContext;

        /// <summary>
        /// Collection of related localization resources for a given language
        /// </summary>
        private ResourceMap resourceMap;

        /// <summary>
        /// Signals the recognition engine to not start initialisation while 
        /// another initialization is already in progress.
        /// </summary>
        public bool isFindingLanguagePacks { get; set; }

        #region Error Handling

        /// <summary>
        /// If the speech pack is not installed for the selected recognition
        /// language, an exception will be thrown with this HResult value
        /// </summary>
        private static uint HResultSpeechPackNotInstalled = 0x8004503a;

        #endregion

        #region Speech Recognizer

        /// <summary>
        /// Handles all speech recognition workflows
        /// </summary>
        public SpeechRecognizer speechRecognizer;

        public bool DidInitializeRecognizer
        {
            get
            {
                return this.speechRecognizer != null;
            }
        }

        public Language CurrentLanguage
        {
            get
            {
                return this.speechRecognizer.CurrentLanguage;
            }
        }

        private async Task ConfigureRecognizer(Language selectedLanguage)
        {
            // Kill any recogniser instance that may be floating out there
            await this.StopRecognizer();

            try
            {
                this.speechRecognizer = new SpeechRecognizer(selectedLanguage);
                this.speechRecognizer.StateChanged += this.RecognizerStateChanged;

                // Configure the grammar file
                string tag = selectedLanguage.LanguageTag;
                string grammarFile = String.Format("Grammar\\{0}\\GemmaDemo.xml", tag);
                StorageFile grammarDefinition = await Package.Current.InstalledLocation.GetFileAsync(grammarFile);
                SpeechRecognitionGrammarFileConstraint grammarConstraint = new SpeechRecognitionGrammarFileConstraint(grammarDefinition);
                speechRecognizer.Constraints.Add(grammarConstraint);
                SpeechRecognitionCompilationResult compilationResult = await this.speechRecognizer.CompileConstraintsAsync();

                // Ensure grammar constraints were compliled successfully
                if (compilationResult.Status != SpeechRecognitionResultStatus.Success)
                {
                    await this.notifyUI("\nUnable to compile grammar.");
                }
                else
                {
                    // the time threshold at which the continuous recognition(dictation) session ends due to lack of audio input
                    this.speechRecognizer.Timeouts.EndSilenceTimeout = TimeSpan.FromSeconds(1.5);

                    // ResultGenerated fires when some recognized phrases occur, or 
                    // the garbage rule is hit
                    this.speechRecognizer.ContinuousRecognitionSession.Completed += SessionCompleted;
                    this.speechRecognizer.ContinuousRecognitionSession.ResultGenerated += RecognizerResultGenerated;
                    this.speechRecognizer.HypothesisGenerated += RecognizerHypothesisGenerated;
                }
            }
            catch (Exception ex)
            {
                if ((uint)ex.HResult == HResultSpeechPackNotInstalled)
                {
                    await this.notifyUI("Speech language pack for selected language is not installed.") ;
                }
                else
                {
                    await this.notifyUI(ex.Message);
                }
            }
        }

        public async Task ReconfigureRecognizer(Language newLanguage)
        {
            // Clean up and reinitialize resource context
            this.resourceContext.Languages = new string[] { newLanguage.LanguageTag };
            await this.ConfigureRecognizer(newLanguage);
        }

        public async Task StartRecognizer()
        {
            if (this.speechRecognizer.State == SpeechRecognizerState.Idle)
            {
                try
                {
                    await this.speechRecognizer.ContinuousRecognitionSession.StartAsync();
                    await this.notifyUI(this.resourceMap.GetValue("GrammarHelpText", this.resourceContext).ValueAsString);
                    UIDelegate.EnableLanguageComboBox = false;
                }
                catch (Exception ex)
                {
                    await this.notifyUI("Start Async Failure");
                    await this.notifyUI(ex.Message);
                }
            }
            else
            {
                try
                {
                    UIDelegate.EnableLanguageComboBox = true;
                    await this.notifyUI(this.resourceMap.GetValue("GrammarHelpText", resourceContext).ValueAsString);
                    // Cancelling recognition prevents any currently recognised speech from generating
                    // a ResultGenerated event. StopAsync() will allow the final session to complete.
                    await this.speechRecognizer.ContinuousRecognitionSession.CancelAsync();
                }
                catch (Exception ex)
                {
                    await this.notifyUI("Cancel Async Failure");
                    await this.notifyUI(ex.Message);
                }
            }
        }

        public async Task StopRecognizer()
        {
            if (this.speechRecognizer != null)
            {
                if (this.speechRecognizer.State != SpeechRecognizerState.Idle)
                {
                    await this.speechRecognizer.ContinuousRecognitionSession.CancelAsync();
                }

                this.speechRecognizer.ContinuousRecognitionSession.Completed -= SessionCompleted;
                this.speechRecognizer.ContinuousRecognitionSession.ResultGenerated -= RecognizerResultGenerated;
                this.speechRecognizer.HypothesisGenerated -= RecognizerHypothesisGenerated;
                this.speechRecognizer.StateChanged -= this.RecognizerStateChanged;

                this.speechRecognizer.Dispose();
                this.speechRecognizer = null;
            }
        }

        private void SessionCompleted(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionCompletedEventArgs args)
        {
            Debug.WriteLine("Session completed: " + args.Status.ToString());
            UIDelegate.EnableLanguageComboBox = true;
        }

        private async void RecognizerResultGenerated(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionResultGeneratedEventArgs args)
        {
            if (args.Result.Confidence == SpeechRecognitionConfidence.Medium ||
                args.Result.Confidence == SpeechRecognitionConfidence.High)
            {
                await this.notifyUI(args.Result.Text, "UserVoiceInput");
                await this.HandleRecognition(args.Result);
            }
            // Prompt the user if recognition failed or recognition confidence is low.
            else if (args.Result.Confidence == SpeechRecognitionConfidence.Rejected ||
            args.Result.Confidence == SpeechRecognitionConfidence.Low)
            {
                await this.notifyUI(this.resourceMap.GetValue("GarbagePromptText", this.resourceContext).ValueAsString);
            }
        }

        private void RecognizerHypothesisGenerated(SpeechRecognizer sender, SpeechRecognitionHypothesisGeneratedEventArgs args)
        {
            string hypothesis = args.Hypothesis.Text;
            Debug.WriteLine(hypothesis + " ...");
        }

        private async Task HandleRecognition(SpeechRecognitionResult result)
        {
            var responseKey = result.SemanticInterpretation.Properties["ResponseKey"][0].ToString();
            var responseText = this.resourceMap.GetValue(responseKey, this.resourceContext).ValueAsString;

            await this.notifyUI(responseText);
        }

        /// <summary>
        /// Communicates the recognizer status to the UI.
        /// Use args.State.ToString() to get a text description.
        /// </summary>
        /// <param name="sender">The designated speech recognizer</param>
        /// <param name="args">Recognizer's current stat</param> 
        void RecognizerStateChanged(SpeechRecognizer sender, SpeechRecognizerStateChangedEventArgs args)
        {
            Debug.WriteLine(args.State.ToString());
        }

        #endregion
    }
}