﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.Media.SpeechRecognition;
using Windows.Globalization;
using Windows.UI.Popups;
using System.Diagnostics;
using Windows.UI.Core;
using System.Threading.Tasks;
using Windows.Media.SpeechSynthesis;

namespace Gemma
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page, ISpeechProviderUIDelegate
    {
        private static SpeechEngine speechEngine = new SpeechEngine();

        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedFrom(NavigationEventArgs e)
        {
            await speechEngine.StopRecognizer();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e) {
            base.OnNavigatedTo(e);
            speechEngine.UIDelegate = this;
            await speechEngine.Setup(CoreWindow.GetForCurrentThread().Dispatcher);
        }

        private async void LangSelectionBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (speechEngine.isFindingLanguagePacks) {
                return;
            }

            StartButton.IsEnabled = false;

            ComboBoxItem item = (ComboBoxItem)(LangSelectionBox.SelectedItem);
            Language newLanguage = (Language)item.Tag;
            if (speechEngine.DidInitializeRecognizer) {
                if (speechEngine.CurrentLanguage == newLanguage) {
                    return;
                }
            }

            // trigger cleanup and re-initialisation of speech
            try {
                await speechEngine.ReconfigureRecognizer(newLanguage);
            }
            catch (Exception exception) {
                var messageDialog = new MessageDialog(exception.Message, "Exception");
                await messageDialog.ShowAsync();
            }

            StartButton.IsEnabled = true;
        }

        /// <summary>
        /// Begin recognition or finish the recognition session.
        /// </summary>
        /// <param name="sender">The button that generated this event</param>
        /// <param name="e">Unused event details</param>
        public async void ContinuousRecognize_Click(object sender, RoutedEventArgs e)
        {
            StartButton.IsEnabled = false;
            await speechEngine.StartRecognizer();
            StartButton.IsEnabled = true;
        }

        #region ISpeechProviderUIDelegate

        public bool EnableLanguageComboBox { get; set; }

        public void UpdateLanguageComboBox(IEnumerable<Language> languages, Language defaultLanguage)
        {
            foreach (Language language in languages)
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Tag = language;
                item.Content = language.DisplayName;

                LangSelectionBox.Items.Add(item);
                if (language.LanguageTag == defaultLanguage.LanguageTag)
                {
                    item.IsSelected = true;
                    LangSelectionBox.SelectedItem = item;
                }
            }
        }

        public async void HandleSpeechEngineResponse(string phrase)
        {
            GemmaTextBlock.Text = phrase;
            MediaElement mediaElement = this.mediaElement;
            var synth = new Windows.Media.SpeechSynthesis.SpeechSynthesizer();
            SpeechSynthesisStream stream = await synth.SynthesizeTextToStreamAsync(phrase);
            mediaElement.SetSource(stream, stream.ContentType);
            mediaElement.Play();
        }

        public void HandleUserVoiceInput(string phrase)
        {
            UserTextBlock.Text = phrase;
        }

        #endregion
    }
}