﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Globalization;
using Windows.Media.SpeechRecognition;

namespace Gemma
{
    interface ISpeechProviderUIDelegate
    {
        void UpdateLanguageComboBox(IEnumerable<Language> languages, Language defaultLanguage);

        bool EnableLanguageComboBox { get; set; }

        void HandleUserVoiceInput(string phrase);

        void HandleSpeechEngineResponse(string phrase);
    }
}
