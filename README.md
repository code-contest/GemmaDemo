# Gemma Engine

`GemmaEngine` is a custom-grammar based speech recognition engine powered by the built-in 
speech recognizer in Windows([`Windows.Media.SpeechRecognition.SpeechRecognizer`](https://docs.microsoft.com/en-us/uwp/api/windows.media.speechrecognition.speechrecognizer)).

## Persons of Interest

| File                           | Purpose                                                          |
| ------------------------------ | ---------------------------------------------------------------- |
| `AudioCapturePermissions.cs`   | Manages privacy permissions for microphone access                |
| `SpeechEngine.cs`              | Manages the speech recognition process                           |
| `ISpeechProviderUIDelegate.cs` | Interface to be implemented by the UI code                       |
| `Grammar/en-GB/GemmaDemo.xml`  | Custom grammar declaration compliant with SRGS Specification 1.0 |
| `Strings/en-GB/LocalizationSpeechResources.resw` | Declares script responses and prompts          |

## How to make it work with your code

1. Copy all the aforementioned files in the corresponding directories in your project
2. Make your `Page` class adopt `ISpeechProviderUIDelegate.cs`. For examples, see `MainPage.xaml.cs`.
3. Initialise an instance of `SpeechEngine` in the `OnNavigatedTo()` method. For example, see `MainPage.xaml.cs`
4. Make sure recognition is killed in `OnNavigatedFrom()`. See `MainPage.xaml.cs`.

That's all. Your app should work perfectly now. 



    

